import os
import json

from flask import Flask, request,Response

app = Flask(__name__)

@app.route("/test/", methods=['GET'])
def test():
	environment = os.getenv("environment","Development")
	return f"<h1>Critical Hello World Application - <small>{environment.upper()}</small></h1>"
	# return Response(json.dumps({"message":f"Critical Flask Application Response from {environment}"}),status=200)


if __name__ == "__main__":
	app.run(debug=True, host="0.0.0.0", threaded=True,port=8021)
